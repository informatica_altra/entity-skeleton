<?php

namespace Altra\EntitySkeleton\Commands\Generates;

use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Pluralizer;

class GenerateCreateActionCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'generate:create-action';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create action from stub';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Action';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__ . '/../../../stubs/' . $this->argument('name') . '/AmqpConsumerActions/CreateAction.stub';
    }

    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . '\Actions\AmqpConsumers\\' . $this->argument('name');
    }

    protected function getNameInput()
    {
        return 'Create' . $this->getSingularClassName($this->argument('name')) . 'Action';
    }

    private function getSingularClassName($name)
    {
        return ucwords(Pluralizer::singular($name));
    }
}

<?php

namespace Altra\EntitySkeleton\Commands\Generates;

use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Pluralizer;

class GenerateUpdateActionCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'generate:update-action';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create update action from stub';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Action';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__ . '/../../../stubs/' . $this->argument('name') . '/AmqpConsumerActions/UpdateAction.stub';
    }

    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . '\Actions\AmqpConsumers\\' . $this->argument('name');
    }

    protected function getNameInput()
    {
        return 'Update' . $this->getSingularClassName($this->argument('name')) . 'Action';
    }

    private function getSingularClassName($name)
    {
        return ucwords(Pluralizer::singular($name));
    }
}

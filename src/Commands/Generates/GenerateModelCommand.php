<?php

namespace Altra\EntitySkeleton\Commands\Generates;

use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Pluralizer;

class GenerateModelCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'generate:model';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new model from stub';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Model';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__ . '/../../../stubs/' . $this->argument('name') . '/Model.stub';
    }

    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . '\Models';
    }

    protected function getNameInput()
    {
        return $this->getSingularClassName($this->argument('name'));
    }

    public function getSingularClassName($name)
    {
        return ucwords(Pluralizer::singular($name));
    }
}

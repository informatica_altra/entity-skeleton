<?php

namespace Altra\EntitySkeleton\Commands\Generates;

use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Pluralizer;

class GenerateDtoCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'generate:dto';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new data transfer object from stub';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Dto';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__ . '/../../../stubs/' . $this->argument('name') . '/Dto.stub';
    }

    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . '\Dto';
    }

    protected function getNameInput()
    {
        return $this->getSingularClassName($this->argument('name')) . 'Data';
    }

    private function getSingularClassName($name)
    {
        return ucwords(Pluralizer::singular($name));
    }
}

<?php

namespace Altra\EntitySkeleton\Commands\Generates;

use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Pluralizer;

class GenerateActionTestsCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'generate:tests';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create tests from stub';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Test';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__ . '/../../../stubs/' . $this->argument('name') . '/Tests/Tests.stub';
    }

    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . "/../tests/Feature/app/actions/" . strtolower($this->argument('name'));
    }

    protected function getNameInput()
    {
        return $this->getSingularClassName($this->argument('name')) . 'ActionsTest';
    }

    public function getSingularClassName($name)
    {
        return ucwords(Pluralizer::singular($name));
    }
}

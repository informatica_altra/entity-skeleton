<?php

namespace Altra\EntitySkeleton\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Pluralizer;

class GenerateSkeletonCommand extends Command
{
    protected $entities = [
    'TranslationLines',
  ];
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:skeleton';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command generates a skeleton for an entity';

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        $entity = $this->choice('¿What entity skeleton you would like to create?', $this->entities);

        //Generate DTO
        $this->info('Generating DTO');
        if (!class_exists("App\Dto\\" . $this->getSingularClassName($entity) . "Data") && file_exists(__DIR__ . '/../../stubs/' . $entity . '/Dto.stub')) {
            $this->call('generate:dto', ['name' => $entity]);
        } else {
            $this->info('The class already exists or there is no stub for it.');
        }

        //Generate model
        $this->info('Generating Model');
        if (!class_exists("App\Models\\" . $this->getSingularClassName($entity)) && file_exists(__DIR__ . '/../../stubs/' . $entity . '/Model.stub')) {
            $this->call('generate:model', ['name' => $entity]);
        } else {
            $this->info('The class already exists or there is no stub for it.');
        }

        //Generate Create Action
        $this->info('Generating CreateAction');
        if (!class_exists("App\Actions\AmqpConsumers\\" . $entity . '\\' . 'Create' . $this->getSingularClassName($entity) . 'Action') && file_exists(__DIR__ . '/../../stubs/' . $entity . '/AmqpConsumerActions/CreateAction.stub')) {
            $this->call('generate:create-action', ['name' => $entity]);
        } else {
            $this->info('The class already exists or there is no stub for it.');
        }

        //Generate Update Action
        $this->info('Generating UpdateAction');
        if (!class_exists("App\Actions\AmqpConsumers\\" . $entity . '\\' . 'Update' . $this->getSingularClassName($entity) . 'Action') && file_exists(__DIR__ . '/../../stubs/' . $entity . '/AmqpConsumerActions/UpdateAction.stub')) {
            $this->call('generate:update-action', ['name' => $entity]);
        } else {
            $this->info('The class already exists or there is no stub for it.');
        }

        //Generate Delete Action
        $this->info('Generating DeleteAction');
        if (!class_exists("App\Actions\AmqpConsumers\\" . $entity . '\\' . 'Delete' . $this->getSingularClassName($entity) . 'Action') && file_exists(__DIR__ . '/../../stubs/' . $entity . '/AmqpConsumerActions/DeleteAction.stub')) {
            $this->call('generate:delete-action', ['name' => $entity]);
        } else {
            $this->info('The class already exists or there is no stub for it.');
        }

        //Generate tests
        $this->info('Generating Tests');
        if (!file_exists(base_path('tests') . "/Feature/app/actions/" . strtolower($entity) . '/' . $this->getSingularClassName($entity) . 'ActionsTest.php') && file_exists(__DIR__ . '/../../stubs/' . $entity . '/Tests/Tests.stub')) {
            $this->call('generate:tests', ['name' => $entity]);
        } else {
            $this->info('The class already exists or there is no stub for it.');
        }
    }

    public function getSingularClassName($name)
    {
        return ucwords(Pluralizer::singular($name));
    }
}

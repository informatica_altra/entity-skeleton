<?php

namespace Altra\EntitySkeleton;

use Altra\EntitySkeleton\Commands\GenerateSkeletonCommand;
use Altra\EntitySkeleton\Commands\Generates\GenerateActionTestsCommand;
use Altra\EntitySkeleton\Commands\Generates\GenerateCreateActionCommand;
use Altra\EntitySkeleton\Commands\Generates\GenerateDeleteActionCommand;
use Altra\EntitySkeleton\Commands\Generates\GenerateDtoCommand;
use Altra\EntitySkeleton\Commands\Generates\GenerateModelCommand;
use Altra\EntitySkeleton\Commands\Generates\GenerateUpdateActionCommand;
use Illuminate\Support\ServiceProvider;

class EntitySkeletonServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any package services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
        GenerateSkeletonCommand::class,
        GenerateDtoCommand::class,
        GenerateModelCommand::class,
        GenerateActionTestsCommand::class,
        GenerateCreateActionCommand::class,
        GenerateDeleteActionCommand::class,
        GenerateUpdateActionCommand::class,
      ]);
        }
    }
}

# Altra Entity Skeleton package

#### This package haves the purpose of creating a skeleton for an altra entity that will be used in multiple microservices.

##### Installation

```
composer require altra/entity-skeleton
```

To generate a skeleton we have to run in our project:

```
php artisan generate:skeleton
```

This command will prompt a question of which entity you want to generate the skeleton and create the classes defined in the package. If any of those classes is already created it will skipp the generation of that class.

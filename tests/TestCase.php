<?php

namespace Altra\EntitySkeleton\Tests;

use Altra\EntitySkeleton\EntitySkeletonServiceProvider;
use Orchestra\Testbench\TestCase as Orchestra;

class TestCase extends Orchestra
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    protected function getPackageProviders($app)
    {
        return [
            EntitySkeletonServiceProvider::class,
        ];
    }
}
